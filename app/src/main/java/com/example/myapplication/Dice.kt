package com.example.myapplication

class Dice(private val sidesSize: Int) {

    fun roll(): Int {
        return (1..sidesSize).random()
    }
}